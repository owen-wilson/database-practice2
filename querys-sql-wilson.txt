





--1

INSERT INTO Persona (persona_id,nombre,apellido,genero,fechaNacimiento,pais) VALUES (1,'rubi','huanca','F','1994-05-06','Bolivia');
INSERT INTO Persona (persona_id,nombre,apellido,genero,fechaNacimiento,pais) VALUES (2,'wilson','mamani flores','M','1992-12-30','Bolivia');
INSERT INTO Persona (persona_id,nombre,apellido,genero,fechaNacimiento,pais) VALUES (3,'lucia','pozo','F','1990-10-26','Bolivia')
INSERT INTO Persona (persona_id,nombre,apellido,genero,fechaNacimiento,pais) VALUES (4,'narrieth','peres','F','1995-11-25','Peru')
INSERT INTO Persona (persona_id,nombre,apellido,genero,fechaNacimiento,pais) VALUES (5,'fabiola','cusi','F','1996-11-29','Bolivia')
INSERT INTO Persona (persona_id,nombre,apellido,genero,fechaNacimiento,pais) VALUES (6,'andrea','juarez','F','1992-02-29','Mexico')
INSERT INTO Persona (persona_id,nombre,apellido,genero,fechaNacimiento,pais) VALUES (7,'micaela','torrico','F','1996-03-10','Argentina');
SELECT * FROM Persona;

--2
INSERT INTO Persona (persona_id,nombre,apellido,genero,fechaNacimiento,pais) VALUES (8,'juan','perez','M','1990-05-30','');
INSERT INTO Persona (persona_id,nombre,apellido,genero,fechaNacimiento,pais) VALUES (9,'marcos','galvez','M','1985-09-10','');
INSERT INTO Persona (persona_id,nombre,apellido,genero,fechaNacimiento,pais) VALUES (10,'alex','pereira','M','1984-05-06','');
--3
INSERT INTO Persona (persona_id,nombre,apellido,genero,fechaNacimiento,pais) VALUES (11,'julia','rodriguez','F',NULL,'');

--4
INSERT INTO Libro (libro_id,titulo,precio,persona_id) VALUES (1,'redes',5.6,2);
INSERT INTO Libro (libro_id,titulo,precio,persona_id) VALUES (2,'algebra',96.3,5);
INSERT INTO Libro (libro_id,titulo,precio,persona_id) VALUES (3,'geometria',45.2,4);
INSERT INTO Libro (libro_id,titulo,precio,persona_id) VALUES (4,'angular',26.3,9);
INSERT INTO Libro (libro_id,titulo,precio,persona_id) VALUES (5,'reactjs',45.2,10);
INSERT INTO Libro (libro_id,titulo,precio,persona_id) VALUES (6,'nodejs',78.2,1);
INSERT INTO Libro (libro_id,titulo,precio,persona_id) VALUES (7,'django',23.2,3);
INSERT INTO Libro (libro_id,titulo,precio,persona_id) VALUES (8,'programcion',78.2,5);
INSERT INTO Libro (libro_id,titulo,precio,persona_id) VALUES (9,'sqlserver',15.2,9);
INSERT INTO Libro (libro_id,titulo,precio,persona_id) VALUES (10,'mysql',56.2,4);

SELECT * FROM Libro;

--5
SELECT apellido,nombre FROM Persona;

--6
SELECT * FROM Libro WHERE precio > 50  AND titulo LIKE 'A%'

--7
SELECT nombre,apellido,precio FROM Persona,Libro WHERE precio > 50;


--8
SELECT nombre, apellido FROM Persona WHERE fechaNacimiento BETWEEN '1990-01-01' AND '1990-12-31';

--9
SELECT * FROM Libro WHERE titulo like '_e%' 

--10
SELECT count(*) FROM Persona

--11
SELECT MIN (precio) MinPrecio FROM Libro

--12
SELECT MAX (precio) MaxPrecio FROM Libro;

--13
SELECT SUM(precio) / COUNT (*) FROM Libro;

--14
SELECT SUM(precio) FROM Libro;

--15
ALTER TABLE Libro ALTER COLUMN titulo varchar(300);

--16
SELECT  * FROM Persona WHERE pais='Colombia';
SELECT  * FROM Persona WHERE pais='Mexico';

--17
SELECT * FROM Libro WHERE NOT LEFT(titulo, 1)='T';

--18
SELECT * FROM Libro WHERE precio >=50 AND precio <=70;

--19
SELECT * FROM Persona WHERE pais='bolivia';
SELECT * FROM Persona WHERE pais='Colombia';
SELECT * FROM Persona WHERE pais='Peru';
SELECT * FROM Persona WHERE pais='Mexico';

--20
SELECT * FROM Persona WHERE pais='bolivia' AND genero = 'F';
SELECT * FROM Persona WHERE pais='Peru' AND genero = 'F';
SELECT * FROM Persona WHERE pais='Bolivia' AND genero = 'M';

--21
SELECT * FROM Persona WHERE genero = 'F';

--22
SELECT * FROM Libro ORDER BY titulo DESC;

--23
DELETE FROM Libro WHERE titulo = 'sqlserver';

--24
DELETE FROM Libro WHERE titulo LIKE 'A%';

--25
DELETE FROM Persona WHERE persona_id NOT IN (SELECT persona_id FROM Libro);


--26
TRUNCATE TABLE Libro;

--27
UPDATE Persona SET nombre='marco' WHERE persona_id=7;

--28
UPDATE Libro SET precio='57.0' WHERE titulo='algebra'; 


--29
ESTA CONSULTA REALIZA EL CONTEO DE LA CANTIDAD DE LIBROS

SELECT precio , COUNT(*) AS tit from Libro GROUP BY precio;

ESTA CONSULTA HACEMOS UN FILTRO A UN GRUPO DE REGISTRO CON HAVING EN LA TABLA LIBRO
SELECT titulo, avg(precio) FROM Libro WHERE titulo LIKE 'A%' GROUP BY titulo HAVING avg(precio) > 50;
