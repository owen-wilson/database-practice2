CREATE SCHEMA dbo;

CREATE TABLE Persona ( 
	persona_id           smallint NOT NULL   ,
	nombre               varchar(20) NOT NULL   ,
	apellido             varchar(20) NOT NULL   ,
	genero               char(1) NOT NULL   ,
	fechaNacimiento      date    ,
	pais                 varchar(20)    ,
	CONSTRAINT Pk_Persona_persona_id PRIMARY KEY  ( persona_id )
 );

CREATE TABLE Libro ( 
	libro_id             smallint NOT NULL   ,
	titulo               varchar(300)    ,
	precio               decimal(5,2)    ,
	persona_id           smallint NOT NULL   ,
	CONSTRAINT Pk_Libro_libro_id PRIMARY KEY  ( libro_id )
 );

CREATE  INDEX Idx_Libro_persona_id ON Libro ( persona_id );

ALTER TABLE Libro ADD CONSTRAINT fk_libro_persona FOREIGN KEY ( persona_id ) REFERENCES Persona( persona_id ) ON DELETE NO ACTION ON UPDATE NO ACTION;

